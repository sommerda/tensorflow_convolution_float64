import numpy as np

try:
    import tensorflow as tf
    if tf.__version__ < "1.8.0":
        raise Exception("Require tensorflow >= 1.8.0")
    tensorflow_session = tf.Session()
    tensorflow_dict = {}
    print("[*] Tensorflow is available.")
    TENSORFLOW_AVAILABLE = True
except Exception as e:
    print("[*] Tensorflow not available: {}".format(e))
    TENSORFLOW_AVAILABLE = False


def tensorflow_convolve_float64(x,y, mode='same', DTYPE=tf.float64):
    assert len(x) == len(y)
    assert len(x) % 2 == 1  # require x it to be odd in length
    assert mode in ('same', 'full')
    length = len(x)
    key = "{}-{}".format(length, mode)
    try:
        out_tensor, x_tensor, y_tensor = tensorflow_dict[key]
    except:
        print("Create new tensorflow model")
        LENGTH = length

        x_tensor = tf.placeholder(DTYPE, shape=(LENGTH), name='x')
        y_tensor = tf.placeholder(DTYPE, shape=(LENGTH), name='x')
        y_rev_tensor = tf.reverse(x_tensor, axis=[0])

        x_tensor_4D = tf.reshape(x_tensor, (1,1, LENGTH, 1))
        y_rev_tensor_4D = tf.reshape(y_rev_tensor, (1,LENGTH, 1, 1))

        if mode == 'full':
            x_tensor_4D = tf.pad(tensor=x_tensor_4D, paddings=((0,0), (0,0), (LENGTH // 2, LENGTH // 2), (0,0)), mode="CONSTANT")

        out_tensor = tf.nn.conv2d(x_tensor_4D, y_rev_tensor_4D, strides=(1,1,1,1), padding='SAME')[0,0,:,0]

        tensorflow_dict[key] = (out_tensor, x_tensor, y_tensor)

    res = tensorflow_session.run(out_tensor, feed_dict={x_tensor: x, y_tensor: y})
    return res

# this seems to be very inprecise
def tensorflow_convolution_direct_same(x,y):
    assert len(x) == len(y)
    LENGTH = len(x)
    DTYPE = tf.float64

    x_tensor = tf.placeholder(DTYPE, shape=(LENGTH), name='x')
    y_tensor = tf.placeholder(DTYPE, shape=(LENGTH), name='x')
    y_rev_tensor = tf.reverse(y_tensor, axis=[0])

    x_tensor_rs = tf.reshape(x_tensor, (1, LENGTH, 1))
    y_rev_tensor_rs = tf.reshape(y_rev_tensor, (LENGTH, 1, 1))

    print(x_tensor_rs.shape, y_rev_tensor_rs.shape)

    out_tensor = tf.nn.convolution(input=x_tensor_rs, filter=y_rev_tensor_rs, padding='SAME')

    return tensorflow_session.run(out_tensor, feed_dict={x_tensor: x.reshape(LENGTH), y_tensor: y.reshape((LENGTH))}).reshape(LENGTH)


def test(mode='full'):
    a = np.random.rand(100001)
    a /= np.sum(a)

    res1 = np.convolve(a,a, mode=mode)
    res2 = tensorflow_convolve_float64(a,a, mode=mode, DTYPE=tf.float64)

    for g,h in zip(res1, res2):
        if g != h:
            print("{:.50f} : {:.50f}".format(g,h))

    print("are equal: {}".format(np.array_equal(res1, res2)))
    print("abs difference: {}".format(np.sum(np.abs(res1 - res2))))

if __name__ == "__main__":
    test(mode="full")
