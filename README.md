This is a python code snipped I write to mimic numpy's numpy.convolve in TensorFlow for float64. It supports the numpy modes 'full' and 'same', but only arrays with odd length. Moreover, some kind of graph caching functionality is added, so that the tensorflow graph does not need to be rebuilt every time the function is called. 

Requires tensorflow.VERSION > 1.8.0

On my hardware, somehow the result seems to deviate slightly from the same operation in numpy. Maybe tf.conv2d uses internally somewhere still 32bit. 
The tensorflow convolution of a random array of length 100001 with sum(array) = 1.0 differs by ~ 10^-16:

np.sum(np.abs(convolved_array_numpy - convolved_array_tf)) = 10^-16

This is IMHO too much. See the included test function for details. 